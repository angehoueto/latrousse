---
title: Installer RIT
keywords: comment, installer, rit, rotman, interactive, trader, logiciel, simulation, simulateur,boursière
weight: 0
---

Le logiciel *Rotman Interactive Trader* (RIT) peut être installé sur **Windows**. Si vous travaillez sur **MAC**, il est recommandé d'installer Windows 10 sur votre MAC avec le *Boot camp Assistant* {{%extlink "https://support.apple.com/fr-ca/HT201468"%}}.

## Télécharger le logiciel

{{%tabs%}}
{{%tab "Instructions"%}}

1. Allez sur le site officiel de RIT à l'adresse `https://rit.rotman.utoronto.ca/` et cliquez sur `Downloads` {{%extlink "https://rit.rotman.utoronto.ca/software.asp"%}}.

2. Téléchargez et installez le fichier `Download 1 (RIT Client)`, soit le logiciel `RIT`.

3. Téléchargez et installez le fichier `Download 2 (Excel RTD Links)`. Ceci vous permettra de connecter `RIT` à `Excel` ainsi qu'à tout langage de programmation.

{{%notice info%}}
Il y a deux fichiers disponibles et vous devez choisir celui correspondant à votre version d'Excel (`32-bit vs 64-bit`). Pour vérifier, cliquez sur `Fichier > Compte > À propos de Excel`.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_install.gif)

{{%/tab%}}
{{%/tabs%}}

## Tester l'installation

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lancez `RIT` et connectez-vous avec le nom d'utilisateur, mot de passe, serveur et port transmis par votre enseignant. Si vous n'avez pas encore reçu ces informations, vous pouvez quand même vous connecter au serveur `demo` de l'Université de Toronto {{%extlink "https://rit.rotman.utoronto.ca/demo.asp"%}}.

2. Une fois connecté à `RIT` cliquez sur `Portfolio` et ensuite lancez `Excel`.

3. Glisser-déposez un champ quelconque à partir du `Portfolio` vers `Excel`, par exemple le prix `Last`. Les données devraient se mettre à jour sur `Excel` au fur et à mesure que la simulation avance.

{{%notice info%}}
Si les données ne se mettent pas à jour sur `Excel`, assurez-vous d'avoir installé la bonne version du fichier `Download 2 (Excel RTD Links)`. Si ceci est déjà fait, vérifiez si votre ordinateur satisfait la configuration minimale requise détaillée sur le site officiel de `RIT` . Souvent c'est le `Microsoft .NET 4.0 Framework` qui doit être installé {{%extlink "https://rit.rotman.utoronto.ca/software.asp"%}}.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_test.gif)

{{%/tab%}}
{{%/tabs%}}
