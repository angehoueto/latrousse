---
title: Getting Started
keywords: launch, rit, connect, login, chart, portfolio, trader, info
description: Take a quick tour through the Rotman Interactive Trader (RIT) market simulator.
weight: 0
---

This is a quick introduction to the modules available on the Rotman Interactive Trader (RIT) market simulator.

## Login

{{%tabs%}}
{{%tab "Instructions"%}}

1. Launch `RIT` and login using  the credentials provided by your instructor.

{{%notice note%}}
Each simulation has a **unique** trader ID, password and port.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_login.gif)

{{%/tab%}}
{{%/tabs%}}

## Quick tour

The `RIT` software lets you customize your workspace with different tools such as :

+ `Portfolio` shows you the available securities on the market and your positions.
+ `Order Entry > Buy/Sell` lets you `buy` and `sell` securities.
+ `Market Depth > Book Trader` shows you the order book, i.e. the market split in two columns : sellers on the right side (`ask`) and buyers on the left side (`bid`).
+ `Trade Blotter` shows you a record of your pending and completed trades.
+ `Transaction Log` shows you a record of all your transactions.
+ `Trader Info` gives you real time information on your profit and loss as well as your trading limits.
+ `Charting > Security Chart` helps you visualize the price of the available securities.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on `Portfolio` and drag and drop the window anywhere you wish.
2. Click on `Order Entrey > Buy/Sell`, you can also click on the `+` sign if you wish to have multiple `Buy/Sell` buttons.
3. Click on `Market Depth > Book Trader`.
4. Click on `Trade Blotter`.
5. Click on `Transaction Log`.
6. Click on `Trader Info`.
7. Click on `Charting > Security Chart`.
8. Position all windows anywhere you wish!

{{%notice tip%}}
The official **Client Software Feature Guide** has a lot more details on each available module and you should read it to get a deeper understanding of the software {{%extlink "https://306w.s3.amazonaws.com/rit_cases/Help/Student/RIT%20-%20User%20Guide%20-%20Client%20Software%20Feature%20Guide.pdf"%}}.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_tour.gif)

{{%/tab%}}
{{%/tabs%}}

## Case files

You will find a file called `Case Brief` explaining the simulation in detail through the `Case Files` module. Some simulations also include additional `Excel` support files.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on `Case Files`.
2. Click on any of the available links.
3. Save a copy of the file as needed.

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_files.gif)

{{%/tab%}}
{{%/tabs%}}
