---
title: "RIT"
date: 2020
icon: "ti-bar-chart-alt"
description: Rotman Interactive Trader (RIT) is a market simulator developped by the Rotman School of Management, University of Toronto.
keywords: rit, software, simulation, stock, market, rotman, interactive, trader, club, simulation, ritc, international, competition
type : "docs"
---

