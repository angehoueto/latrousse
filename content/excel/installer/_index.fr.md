---
title: Installation
keywords: installer excel, installer office, office en ligne, excel web, en ligne, office 365
description: Installation de la dernière version d'Excel.
weight: -1
---

# Office 365

## Connexion web et installation

{{%notice info%}}
Ces instructions d'installation requièrent un compte Microsoft Office 365. Les étudiants et personnel de l'Université Laval <a href= "https://www4.fsa.ulaval.ca/etudiants-actuels/services-technologiques/logiciels/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> peuvent utiliser leur `IDUL` suivi de `@ulaval.ca` pour se connecter.
{{%/notice%}}

1. Aller sur la page de connexion d'Office 365 <a href="https://portal.office.com" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.

2. Se connecter à l'aide de son compte `IDUL@ulaval.ca`.

![`portal.office.com`](login.png)

3. Cliquer sur `Installer`.

![`Accueil Office 365`](home.png)


![`Installer`](install.png)

{{%notice note%}}
Suivre les instructions une fois le téléchargement aura fini.
{{%/notice%}}

## Connexion via Excel

1. Lancer `Excel`sur **l'ordinateur**.
2. Cliquer sur `Compte > Connexion`.

![`Accueil Excel`](excel_home.png)

3. Se connecter à l'aide de **`IDUL@ulaval.ca`**.

![`Login Excel`](excel_login.png)

![`Login UL`](excel_ul_login.png)

4. Vérifier qu'Excel soit connecté au compte **`@ulaval.ca`**.

![`Connexion OK`](excel_ok.png)

{{% notice tip%}}
Une fois connecté, vous pouvez accéder à vos fichiers OneDrive directement à partir d'Excel (`CTRL + O`).
{{% /notice %}}

## Ouvrir un fichier OneDrive

1. Lancer `Excel`.
2. Cliquer sur `Fichier > Ouvrir` ou faire la combinaison `CTRL + O`.

![`Ouvrir`](ouvrir.png)

3. Naviguer jusqu'au fichier et faire un double clic pour l'ouvrir. Votre nom devrait se trouver en haut à droite.

![`Ouvert`](ouvert_ok.png)


