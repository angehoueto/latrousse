---
title: Fichiers CSV
keywords: lire csv, csv, importer données csv, séparé par virgule, données csv
description: Comment lire un tableau de données CSV sur Excel.
weight: -1
---

Les données obtenus à partir de WRDS <a href="https://wrds-web.wharton.upenn.edu/" target="_blank"><i class="fa fa-external-link-square-alt"></i></a> sont enregistrées en format `.csv`. Ce format ne sauvegarde que les données dites brutes (sans mise en page, formules, etc.). Pour travailler avec un tel fichier, il suffit de l'ouvrir avec `Excel`. Si les configuration régionales utilisées pour produire le fichier `.csv` ( `,` est souvent utilisé pour séparer les colonnes) concordent avec celles de votre ordinateur, les données deviendront automatiquement un tableau `Excel` une fois ouvertes.

Pourtant, si toutes les données se retrouvent sur la colonne `A` une fois ouvertes, il suffit d'indiquer à `Excel` quel caractère a été utilisé pour séparer les colonnes, par exemple `,` au lieu de `;` .

{{%notice tip%}}
La documentation officielle Excel a plus de détails sur l'importation de fichiers texte <a href="https://support.office.com/fr-fr/article/importer-ou-exporter-des-fichiers-texte-txt-ou-csv-5250ac4c-663c-47ce-937b-339e391393ba?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-square-alt"></i></a> ainsi que sur l'outil *Power Query* <a href="https://support.microsoft.com/fr-fr/office/importer-ou-exporter-des-fichiers-texte-txt-ou-csv-5250ac4c-663c-47ce-937b-339e391393ba" target="_blank"><i class="fa fa-external-link-square-alt"></i></a>.
{{%/notice%}}

## Assistant d'importation de texte

{{%tabs%}}
{{%tab "Instructions"%}}

1. Ouvrez le fichier `.csv` avec Excel.
2. Cliquez sur le libellé de la colonne ayant les données, soit la colonne `A`.
3. Cliquer sur `Données > Convertir`.
4. Suivre les instructions de l'assistant de conversion et cliquez sur `Terminer` une fois l'aperçu de données est correct.
5. Sauvegardez une copie du fichier en format `.xlsx` en cliquant sur `Fichier > Enregistrer sous`.

{{%/tab%}}
{{%tab "Excel"%}}

![](csv_excel.gif)

{{%/tab%}}
{{%/tabs%}}


## Power Query

{{%tabs%}}
{{%tab "Instructions"%}}

1. À partir d'un fichier `.xlsx` ouvert, cliquez sur `Données > Obtenir des données > À partir d'un fichier > À partir d'un fichier texte/CSV`.
2. Choisissez le fichier `.csv` que vous désirez importer sur `Excel`.
3. Assurez-vous que le délimiteur soit correct, par exemple `Virgule` et cliquez sur `Charger`.

{{%/tab%}}
{{%tab "Excel"%}}

![](csv_excel_2.gif)

{{%/tab%}}
{{%/tabs%}}
