---
title: "Excel"
date: 2020
icon: "far fa-file-excel"
description: "Microsoft Excel est un choix très populaire pour l'analyse de données autant au privé qu'au public."
type : "docs"
---
