---
title: "R language"
date: 2020
icon: "fab fa-r-project"
description: "Some examples of R code : installation, creating a chart, etc."
keywords: r, r installation, installing r, rstudio, plot, ggplot2
type : "docs"
---

