---
title: Fichiers CSV
keywords: lire fichier csv, csv, fichier csv, lire données, csv r, fichier csv sur r
description: Comment lire un fichier CSV sur R
weight: 0
---

## Importation des données

Pour lire un fichier `.csv` sur `R` on peut utiliser la fonction `read.csv`. Cette fonction prend comme premier argument le nom du fichier, par exemple `monfichier.csv`. Si le délimiteur des données n'est pas une virgule, il suffit de l'indiquer avec l'argument `sep` :

```r
donnees <- read.csv("monfichier.csv", sep=",")
```

{{%tabs%}}

{{%tab "Instructions"%}}

1. Obtenez des prix historiques de `XGD.TO` sur le site `https://ca.finance.yahoo.com/`.

2. Lisez le tableau de données `CSV` avec la fonction `read.csv()` et sauvegardez-le dans une variable nommée `donnees` :

```r
donnees <- read.csv("Downloads/XGD.TO.csv")
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](csv.gif)
{{%/tab%}}

{{%/tabs%}}

