---
title: Installation
keywords: comment, installer, r, rstudio
description: Comment installer R et RStudio.
weight: 0
---

Suivez ces instructions pour installer `R` ainsi que l'environnement de développement `RStudio`.

## Téléchargement {#download}

{{%notice note%}}
R et RStudio sont disponibles sur `MAC`, `Windows` et `Linux`. Vous pouvez utiliser **R** sans **RStudio**, mais pas l'inverse.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}


### R

1. Allez sur le site `https://cran.r-project.org/`.
2. Cliquez sur `Download R` pour votre système d'exploitation.
2. Téléchargez le fichier d'installation recommandé.
3. Ouvrez le fichier téléchargé et suivez les instructions d'installation.

### RStudio

1. Allez sur le site `https://rstudio.com/products/rstudio/download/`.
2. Téléchargez le fichier d'installation pour votre système d'exploitation en cliquant sur `DOWNLOAD`.
3. Ouvrez le fichier téléchargé et suivez les instructions d'installation.

{{%/tab%}}

{{%tab "R"%}}
![](install_r.gif)
{{%/tab%}}


{{%tab "RStudio"%}}
![](install_rstudio.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Pour vous familiariser avec l'interface et les raccourcis de `RStudio`, consultez la documentation <a href="https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf" target="_blank"><i class="fas fa-file-pdf"></i></a>.
{{%/notice%}}


## R ou RStudio

Testons l'installation de `R` ainsi que celle de `RStudio`. Vous pouvez choisir de travailler soit avec l'interface par défaut de R, soit avec RStudio.


{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Lancez R.
2. Rentrez `print("Allô")` dans l'invite de commande.

### RStudio

3. Lancez RStudio.
2. Rentrez `print("Allô")` dans l'invite de commande.

{{%/tab%}}

{{%tab "R"%}}
![](allo_r.gif)
{{%/tab%}}


{{%tab "RStudio"%}}
![](allo_rstudio.gif)
{{%/tab%}}

{{%/tabs%}}

## Paquetages

`R` vous offre plus de **16 000** paquetages via CRAN <a href="https://cran.r-project.org/web/packages/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>. D'autres paquetages sont disponibles sur Github/Gitlab/ Bitbucket/etc.


{{%tabs%}}

{{%tab "Instructions"%}}

1. Cliquez sur l'onglet `Packages`.
1. Cliquez sur `Installer`.
1. Cherchez le paquetage à installer, par exemple `ggplot2`.
1. Cliquez sur `Install`.
1. Importez le paquetage installé avec `library(ggplot2)`.

{{%notice tip%}}
Vous pouvez exécuter directement la commande `install.packages("ggplot2")` pour installer un paquetage quelconque ainsi que plusieurs à la fois comme dans la commande `install.packages(c("ggplot2", "shiny"))`.
{{%/notice%}}

{{%/tab%}}

{{%tab "RStudio"%}}
![](install_package.gif)
{{%/tab%}}

{{%/tabs%}}

### Listes de paquetages

Voici quelques ressources pour trouver des paquetages :

+ Visualisation web avec `htmlwidgets` ([ici](http://gallery.htmlwidgets.org/)).
+ *Machine Learning* ([ici](https://cran.r-project.org/web/views/MachineLearning.html)).
+ Finance ([ici](https://cran.r-project.org/web/views/Finance.html)).
+ Économétrie ([ici](https://cran.r-project.org/web/views/Econometrics.html)).
+ Optimisation ([ici](https://cran.r-project.org/web/views/Optimization.html)).
+ Liste complète par sujet ([ici](https://cran.r-project.org/web/views/)).
+ Liste de paquetages disponibles sur CRAN ([ici](https://cran.r-project.org/web/packages/)).
+ Liste ***Awesome R*** ([ici](https://github.com/qinwf/awesome-R))
+ Liste **RStudio** ([ici](https://support.rstudio.com/hc/en-us/articles/201057987-Quick-list-of-useful-R-packages))

