---
title: ANOVA
keywords: analyse de variance, anova, aov
description: "R : analyse de variance (ANOVA) avec R."
weight: 0
---

Voici les commandes nécessaires pour faire des analyses de variance à un et à plusieurs facteurs.

## 1 Facteur

Le tableau suivant comporte des salaires[^1] classés en 3 colonnes, soit une colonne par secteur d'activité.

[^1]: Données tirées de *Statistics for Business and Financial Economics*, Lee, Lee et Lee (2013), chap. 12, pg. 574.

```r
salaires <- rbind(c(755,520,438),
                  c(712,295,828),
                  c(845,553,622),
                  c(985,950,453),
                  c(1300,930,562),
                  c(1143,428,348),
                  c(733,510,405),
                  c(1189,864,938))
salaires <- as.data.frame(salaires)
colnames(salaires) <- c("Banks", "Utilities", "Computers")
```
```
  Banks Utilities Computers
1   755       520       438
2   712       295       828
3   845       553       622
4   985       950       453
5  1300       930       562
6  1143       428       348
```

On s'intéresse à savoir si la moyenne des salaires est la même pour tous les secteurs d'activité ou si les moyennes sont différentes pour chaque groupe. Voici notre hypothèse nulle :

$$
H_0 : \mu_1 = \mu_2 = \mu_3
$$

La fonction `aov` nous permet répondre à cette question en faisant une analyse de variance. Pour ce faire, c'est important de préparer ses données avec la fonction `stack` pour respecter une structure comme celle du tableau ci-bas.

```r
salaires <- stack(salaires)
```

```
  values   ind
1    755 Banks
2    712 Banks
3    845 Banks
4    985 Banks
5   1300 Banks
6   1143 Banks
```
>Tous les salaire se trouvent maintenant sous la colonne `values`, les noms des secteurs sont maintenant regroupés sous la colonne `ind`.

Nous pouvons maintenant créer un graphique pour mieux visualiser nos données :

```r
boxplot(values ~ ind, data = salaires, col= rainbow(3),
        main = "Salaires pour 3 groupes de hauts dirigeants")
```

![](graphique.png)

Et voici les résultats de l'analyse de variance à un facteur:

```r
aov_out <- aov(values ~ ind, data = salaires)
summary(aov_out)
```
```
            Df  Sum Sq Mean Sq F value  Pr(>F)
ind          2  685129  342565   6.451 0.00655 **
Residuals   21 1115232   53106
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

## 2 Facteurs

En suivant la même procédure que pour l'exemple avec un seul facteur, nous commençons par préparer notre tableau qui contient cette fois-ci des salaires[^2] pour un nombre d'années d'expérience  allant de 1 à 3 ainsi que 4 régions différentes:


[^2]: idem, page 564.

```r
salaires <- as.data.frame(
  rbind(c(16,16.5,19,17,24,25),
        c(21,20.5,20,19,21,22.5),
        c(18,19,21,20.9,22,21),
        c(13,13.5,20,20.8,25,23))
)
colnames(salaires) <- c(1, 1, 2, 2, 3, 3)

salaires <- stack(salaires)
salaires$ind <- substr(salaires$ind, 1, 1)
salaires$region <- as.character(rep(1:4, 6))
```

Ce qui produit un tableau avec trois colonnes, dont deux correspondant aux facteurs `ind` (années d'expérience) et `region`.

```
   values ind region
1    16.0   1      1
2    21.0   1      2
3    18.0   1      3
4    13.0   1      4
5    16.5   1      1
```

Et voici les résultats de l'analyse de variance à deux facteurs :

### Sans interaction

```r
aov_out <- aov(values ~ region + ind, data = salaires)
summary(aov_out)
```
```
            Df Sum Sq Mean Sq F value   Pr(>F)
region       3   7.92    2.64   0.556 0.651072
ind          2 132.90   66.45  13.981 0.000217 ***
Residuals   18  85.55    4.75
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

### Avec interaction

```r
aov_out <- aov(values ~ region * ind, data = salaires)
summary(aov_out)
```
```
            Df Sum Sq Mean Sq F value   Pr(>F)
region       3   7.92    2.64   4.049   0.0334 *
ind          2 132.90   66.45 101.907 2.96e-08 ***
region:ind   6  77.73   12.95  19.867 1.39e-05 ***
Residuals   12   7.82    0.65
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

## Application

Cet exemple utilise des données obtenues à partir de la fonction `EQS` de `Bloomberg`. Les filtres appliqués sont les suivants :

![](eqs.png)

> Il suffit d'exporter les résultats vers `Excel` pour ensuite sauvegarder le tableau en format `CSV`. Ici, seul le tableau vec les données a été enregistré en `CSV`.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lisez et préparez les données sur `R` :

```r
eqs_toronto <- read.csv("~/Downloads/eqs_toronto.csv")

eqs_toronto <- eqs_toronto[!(eqs_toronto$Ind.Subgroup == "N/A"), ]
eqs_toronto <- eqs_toronto[!(eqs_toronto$YTD.Tot.Ret == "#N/A N/A"), ]
eqs_toronto <- eqs_toronto[!(eqs_toronto$GICS.Sector == "N/A"), ]

eqs_toronto$YTD.Tot.Ret <- as.numeric(eqs_toronto$YTD.Tot.Ret)
eqs_toronto$X1M.Tot.Ret <- as.numeric(eqs_toronto$X1M.Tot.Ret)
nb_sectors <- length(unique(eqs_toronto$GICS.Sector))
```

2. Faites des analyses de variance pour chacune des colonnes suivantes :

+ `YTD.Tot.Ret`
+ `RSI..Period.30`
+ `X1M.Tot.Ret`

Avec un et deux facteurs :

+ `GICS.Sector`
+ `Cntry.of.Risk`

{{%/tab%}}


{{%tab "R"%}}

![](aov.gif)

{{%/tab%}}

{{%tab "?"%}}

```r
## GICS
boxplot(YTD.Tot.Ret ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(YTD.Tot.Ret ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## RSI
boxplot(RSI..Period.30 ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(RSI..Period.30 ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## 1 Month
boxplot(X1M.Tot.Ret ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(X1M.Tot.Ret ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## Two way without interactions
aov_out <- aov(RSI..Period.30 ~ GICS.Sector + Cntry.of.Risk, data = eqs_toronto)
summary(aov_out)

## Two way with interactions
aov_out <- aov(RSI..Period.30 ~ GICS.Sector * Cntry.of.Risk, data = eqs_toronto)
summary(aov_out)
```

{{%/tab%}}


{{%/tabs%}}


