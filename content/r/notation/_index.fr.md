---
title: Notation
keywords: syntaxe, r, matrice, vecteur, notation, apprendre r, débuter avec r
description: "Les bases du langage R : vecteurs et matrices"
weight: 0
---

## Vecteurs

Pour créer le vecteur $(2,3,5,7)$ sur `R`, on peut utiliser la fonction `c()` : il suffit de rentrer les nombres à l'intérieur des parenthèses. Par exemple, la commande `c(2,3,5,7)` créera notre vecteur. Par contre, ce vecteur ne sera ni sauvegardé ni nommé par `R` à moins que nous lui assignons un nom. Voilà une première différence entre `Excel`et `R` : sur Excel, chaque cellule a déjà son propre nom, par exemple `A1`, tandis que sur R il faut nommer ce qu'on souhaite utiliser par la suite.

Ainsi, la commande `mon_vecteur <- c(2,3,5,7)` créera le vecteur et lui assignera le nom `mon_vecteur`. Si nous avons besoin d'afficher le contenu de la variable `mon_vecteur`, il suffit de faire appel à la fonction `print()` avec le nom de la variable à afficher comme argument, c'est-à-dire `print(mon_vecteur)`.

{{%tabs%}}

{{%tab "Code"%}}

1. Créez et affichez un vecteur.

```r
c(2,3,5,7)  # Vecteur affiché, mais pas savegardé
mon_vecteur <- c(2,3,5,7)  # C'est recommandé d'utiliser <- au lieu de =
print(mon_vecteur)  # Affiche le contenu de la variable
```

2. Utilisez la fonction `length()` pour connaître la longueur du vecteur:

```r
mon_vecteur <- c(2,3,5,7)  # Assigne le vecteur à la variable mon_vecteur
length(mon_vecteur)  # Affiche la longueur du vecteur
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](vector.gif)
{{%/tab%}}


{{%/tabs%}}


## Matrices

Pour créer une matrice $M_{4 \times 2}$ sur `R`, on utilise la fonction `matrix()`. Pour apprendre davantage sur cette fonction, il suffit d'exécuter la commande `?matrix` , ce qui affichera le fichier d'aide <a href="https://stat.ethz.ch/R-manual/R-devel/library/base/html/matrix.html" target="_blank"><i class="fa fa-external-link-square-alt"></i></a> pour la fonction  `matrix()`.

```r
?matrix  # Ouvrir l'aide de la fonction
```

En lisant l'aide, on apprend que la fonction `matrix()` prend plusieurs arguments (`data, nrow, ncol, etc.`). Par défaut, cette fonction va créer une matrice remplie verticalement, soit par colonnes. Ainsi, pour créer notre matrice $M_{4 \times 2}$ sur `R` nous devons faire appel à la fonction `matrix` en indiquant comme premier argument les données de la matrice (paramètre `data`), suivi du nombre de lignes (`nrow`) et du nombre de colonnes (`ncol`).

{{%tabs%}}

{{%tab "Code"%}}

1. Pour créer une matrice 4 par 2 :

```r
matrix(data=c(2,3,5,7,2,4,3,2), nrow=4, ncol=2)  # La matrice est remplie par colonne
```

Ce résultat n'a pourtant pas été assigné à une variable quelconque (avec le symbole `<-`). Cette matrice est donc affichée mais pas sauvegardée.

2. Pour élever chaque nombre de la matrice au carré, on utilise la même syntaxe que sur `Excel` :

```r
ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)  # Arguments en ordre
ma_matrice^2  # Puissance 2
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](matrix.gif)
{{%/tab%}}


{{%/tabs%}}

## Indexation

Pour accéder au énième élément du vecteur `mon_vecteur`, il suffit d'utiliser les crochets `[]`. Par exemple, la commande `mon_vecteur[3]` retourne le 3e élément du vecteur `mon_vecteur`. D'ailleurs, cette même syntaxe nous permet d'accéder à l'élément se trouvant dans la ligne $i$, colonne $j$, d'une matrice : il suffit de séparer la ligne et la colonne par une virgule. Par exemple, la commande `ma_matrice[3,2]` retourne l'élément correspondant à la ligne `3`, colonne `2`, de la matrice `ma_matrice`.

```r
mon_vecteur <- c(2,3,5,7)
mon_vecteur[3]  # Affiche 5

ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)
ma_matrice[3,2]  # Affiche 3
```

> Il est possible de choisir plusieurs lignes/colonnes d'une matrice : il suffit d'indiquer les positions à l'aide d'un vecteur. Par exemple, la commande `ma_matrice[c(1, 3), c(1, 2)]` retourne la première et troisième ligne de la matrice (lignes = `c(1,3)`), colonnes 1 et 2 (colonnes = `c(1, 2)`).


