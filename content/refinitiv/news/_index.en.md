---
title: News
keywords: how, to, finds, news, eikon, thomson, reuters, ticker, refinitiv
description: How to find news using Refinitiv Workspace.
weight: 0
---

Follow these instructions to find news concerning any financial security.

## NEWS

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to the `NEWS` APP.
2. Set a minimum and maximum date.
3. Click on the news you'd like to read.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](news.gif)

{{%/tab%}}
{{%/tabs%}}


