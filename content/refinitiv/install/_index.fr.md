---
title: Installation
keywords: comment, installer, refinitiv, workspace, eikon, thomson, reuters
weight: 0
---

<a title = "Compte Refinitiv" href="https://my.refinitiv.com/productregistration.html" target="_blank"><span style="color: var(--link-color);"><i class="fas fa-user-check"></i></span> Création de compte Refinitiv</a>

Avant de commencer, vous avez besoin d'un compte pour pouvoir accéder à `Refinitiv Workspace`. Vous pouvez faire une demande de création de compte [ici](https://my.refinitiv.com/productregistration.html) avec votre adresse courriel institutionnelle, par exemple `@ulaval.ca`.

## Téléchargement {#download}

{{%notice note%}}
Le logiciel est disponible sur `MAC` et `Windows`. Pourtant, la version MAC n'inclut pas encore `Datastream`.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Allez sur le site `https://workspace.refinitiv.com/`.
2. Téléchargez le fichier d'installation pour votre système d'exploitation en cliquant sur `Download`.
3. Ouvrez le fichier téléchargé et suivez les instructions d'installation.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](install_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}

### Connexion

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lancez le logiciel `Refinitiv Workspace`.
2. Connectez-vous avec votre courriel institutionnel, par exemple `@ulaval.ca`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](connect_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}

## Complément Excel

Le complément Excel est installé automatiquement.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lancez `Excel`.
2. L'onglet `Refinitiv 365` apparaît maintenant sur votre Excel.

{{%/tab%}}
{{%tab "Excel"%}}

![](excel_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}

## Version web

Suivez les instructions suivantes pour vous connecter à la version web de *Refinitiv Workspace* .

{{%tabs%}}
{{%tab "Instructions"%}}

1. Allez sur le site `https://workspace.refinitiv.com/`.
2. Cliquez sur `Open in Web`.

{{%/tab%}}
{{%tab "Excel"%}}

![](web.gif)

{{%/tab%}}
{{%/tabs%}}
