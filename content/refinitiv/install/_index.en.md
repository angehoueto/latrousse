---
title: Installation
keywords: how, to, install, eikon, thomson, reuters, refinitiv, workspace
weight: 0
---

<a title = "Refinitiv Account" href="https://my.refinitiv.com/productregistration.html" target="_blank"><span style="color: var(--link-color);"><i class="fas fa-user-check"></i></span> Create a Refinitiv account </a>

You will need a `Refinitiv Workspace` account first. You can self register [here](https://my.refinitiv.com/productregistration.html) using your institutional e-mail, e.g. `@ulaval.ca`.

## Download

{{%notice note%}}
The software is available for `MAC` and `Windows`. However, `Datastream` is only available on `Windows`.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to `https://workspace.refinitiv.com/`.
2. Download the installation file for your operating system by clicking on `Download`.
3. Open the downloaded file and follow the installation instructions.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](install_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}

### Connection

{{%tabs%}}
{{%tab "Instructions"%}}

1. Launch `Refinitiv Workspace`.
2. Connect using your institutional e-mail address, e.g. `@ulaval.ca`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](connect_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}

## Excel Add-in

The Excel Add-in is installed automatically.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Launch `Excel`.
2. The `Refinitiv 365` tab has been added to your Excel.

{{%/tab%}}
{{%tab "Excel"%}}

![](excel_refinitiv.gif)

{{%/tab%}}
{{%/tabs%}}


## Web version

Follow these instructions to log onto the web version of *Refinitiv Workspace* .

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to `https://workspace.refinitiv.com/`.
2. Click on `Open in Web`.

{{%/tab%}}
{{%tab "Excel"%}}

![](web.gif)

{{%/tab%}}
{{%/tabs%}}
