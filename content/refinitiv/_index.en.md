---
title: "Refinitiv Workspace"
date: 2020
icon: "ti-signal"
description: The Refinitiv Workspace (former Eikon) financial analysis and trading software offers access to real-time and historical data through an intuitive interface powered by Reuters.
keywords: eikon, software, terminal, workspace, refinitiv
type : "docs"
---

