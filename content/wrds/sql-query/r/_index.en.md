---
title: R
keywords: r sql, csv, export r data, date format, r date, r wrds, wrds r
description: "WRDS : SQL query using R."
weight: 0
---

We will use `SQL` syntax to query the `WRDS` database and extract a `subset` of the `FRB.FX_MONTHLY` table using the `RPostgres` package .

{{%notice note%}}
You can find more details about the `RPostgres` package on the official website {{%extlink "https://github.com/r-dbi/RPostgres"%}} or at the `PostgreSQL` documentation website {{%extlink "https://www.postgresql.org/docs/manuals/"%}}.
{{%/notice%}}

# Monthly currency data

This sample code creates a `data.frame` named `myforex` by selecting the `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` and `exukus` columns from the `FRB.FX_MONTHLY` table which is accessible via the `wrds` connection created through the `.Rprofile` {{%extlink "https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html"%}} configuration file, this allows us to access the table using the `FRB.FX_MONTHLY` syntax.

```r
library(RPostgres)  # Connection config on .Rprofile
myquery <- c(
  "SELECT date , excaus , exchus , exhkus , exinus , exjpus ,
  exkous ,exmxus , exukus FROM FRB.FX_MONTHLY
  WHERE date > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myforex <- dbFetch(res)  # n argument controls number of records to retrieve
dbClearResult(res)

head(myforex, 5)  # First 5 observations
summary(myforex)
```

The `WHERE` clause filters the dataset to return the lines where the `date` column is greater than `2000-01-01` (**yyyy-mm-dd**).

{{% tabs %}}

{{% tab "Terminal" %}}
![](rquery_forex.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](rquery_forex_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

# Changing de date format

Just like `Excel` and `SAS`, `R` stores dates {{%extlink "https://www.stat.berkeley.edu/~s133/dates.html"%}} as `numeric` values internally. We can control the way a `Date` value is displayed by using the appropriate `format` <a href="https://stat.ethz.ch/R-manual/R-devel/library/base/html/format.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> which outputs a `character` string.

```r
myforex$datestring <- format(myforex$date, "%d/%m/%Y")

head(myforex, 5)
```

Executing the above code will change the date `format` to `%d/%m/%Y` and save it as a `character` variable named `datestring` as shown in the results below.

{{% tabs %}}

{{% tab "Terminal" %}}
![](date_format.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](date_format_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

# Exporting to csv
If you wish to keep a copy of a data table, you can use the `write.csv` function <a href="https://stat.ethz.ch/R-manual/R-devel/library/utils/html/write.table.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> as in the following example which will save a copy of the `myforex` dataset as `forex_prices.csv` in the `~/example` folder :

```r
write.csv(myforex, "~/example/forex_prices.csv")  # write.csv2 uses ";" as separator
```

{{% tabs %}}

{{% tab "Terminal" %}}
![](csv_export.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](csv_export_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}


# Full example
The following `R` commands will create three `.csv` files out of three `wrds` datasets using the `RPostgres` package. The resulting datasets contain currency rates from the `FRB.FX_MONTHLY` table, FTSE Indices prices from the `COMP.G_IDX_MTH` table and FTSE Indices names by `gvkeyx` from the `COMP.G_NAMES_IX` table.


{{% tabs %}}

{{% tab "Terminal" %}}
![](full.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](full_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

The resulting `CSV` files can then be opened using any software.
![](csv_view.png)

## R Code

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Create the `myforex` table :

```r
library(RPostgres)
myquery <- c(
  "SELECT date , excaus , exchus , exhkus , exinus , exjpus ,
  exkous ,exmxus , exukus FROM FRB.FX_MONTHLY
  WHERE date > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myforex <- dbFetch(res)
dbClearResult(res)
```

2. Modify the `date format` if needed :

```r
myforex$date <- format(myforex$date, "%d/%m/%Y")
```

3. Export the `myforex` table as `forex_prices.csv` :

```r
write.csv(myforex, "~/example/forex_prices.csv")
```

{{% /tab %}}

{{% tab "index_names.csv" %}}


1. Create the `gvnames` table :


```r
library(RPostgres)
myquery <- c(
  "SELECT * FROM COMP.G_NAMES_IX WHERE gvkeyx IN ('150004', '150979', '150984', '150994')")

res <- dbSendQuery(wrds, myquery)
gvnames <- dbFetch(res)
dbClearResult(res)
```
2. Export the `gvnames` table as `index_names.csv` :

```r
write.csv(gvnames, "~/example/index_names.csv")
```
{{% /tab %}}

{{% tab "index_prices.csv" %}}


1. Create the `myindex` table :

```r
library(RPostgres)
myquery <- c(
  "SELECT gvkeyx, datadate, prccm, prchm, prclm
  FROM COMP.G_IDX_MTH
  WHERE gvkeyx IN ('150004', '150979', '150984', '150994')
  AND datadate > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myindex <- dbFetch(res)
dbClearResult(res)
```

2. Export the `myindex` table as `index_prices.csv` :

```r
write.csv(myindex, "~/example/index_prices.csv")
```

{{% /tab %}}

{{% /tabs %}}


