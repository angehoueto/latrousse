---
title: SAS SQL
keywords: sas sql, csv, exporter données sas, format de date, date sas, wrds, sas wrds, sas studio
description: "WRDS : Requête SQL via SAS Studio."
weight: 0
---

Nous allons utiliser la syntaxe `SQL` pour appliquer des filtres et extraire certaines données du tableau `FRB.FX_MONTHLY` disponible sur `WRDS`.

{{%notice note%}}
Vous trouverez plus de détails sur la procédure `SAS SQL` dans la documentation officielle de SAS <a href="https://support.sas.com/documentation/cdl//en/sqlproc/69822/HTML/default/viewer.htm#p07v6ho0hymhfvn1jboqfe38jnox.htm" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> .
{{%/notice%}}

# Données sur les taux de change
L'exemple de requête ci-bas permet de créer le tableau `myforex` en choisissant les colonnes `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` et `exukus` du tableau `FRB.FX_MONTHLY` lequel a été chargé au préalable dans l'espace de travail de SAS/Studio, ce qui nous permet d'utiliser la syntaxe `FRB.FX_MONHTLY` pour y référer.

```
PROC SQL;
     CREATE TABLE myforex AS
     SELECT 'date'n , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus
     FROM FRB.FX_MONTHLY
     WHERE date > '01JAN2000'd;
QUIT;

PROC PRINT DATA=myforex (OBS=5);
RUN;
PROC MEANS DATA=myforex;
RUN;
```
La ligne `WHERE` permet de filtrer pour n'obtenir que les données dont la valeur de la colonne `date` est supérieure à `01JAN2000`. Notez que la lettre `d` après l'apostrophe `'` est importante car cela indique à `SAS` qu'il s'agit d'une date et non d'une simple chaine de caractères. En omettant la `d` vous recevrez une erreur indiquant un problème causé par `different data types`. Enfin, les `PROC PRINT` et `PROC MEANS` nous permettent de visualiser les premières 5 lignes (`OBS=5`) du tableau `myforex` ainsi que la moyenne de chaque colonne dans l'onglet `RESULTS` :

![](comp_forex.png)

# Changer le format des dates

Comme c'est le cas avec `Excel`, `SAS` sauvegarde les dates comme variables `numériques`. Pour contrôler comment la valeur de la colonne `date` est affichée, il suffit de modifier le `format` en conséquence <a href="https://documentation.sas.com/?docsetId=ds2pg&docsetTarget=p0bz5detpfj01qn1kz2in7xymkdl.htm&docsetVersion=3.1&locale=en" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> .

```
DATA myforex2;
  SET myforex;
  FORMAT date DDMMYY10.;
RUN;

PROC PRINT DATA=myforex2 (OBS=5);
RUN;
```

L'exécution du code ci-haut change le `format` de la colonne `date` à `DDMMYY10.` et imprime les 5 premières lignes du tableau résultant (`myforex2`).

![](date_format.png)

# Exporter à CSV
Il est possible de créer une copie des données avec la procédure `PROC EXPORT` <a href="https://documentation.sas.com/?docsetId=proc&docsetTarget=n0qarv79909fdhn13c60hfubqwdq.htm&docsetVersion=9.4&locale=en" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.  L'exemple ci-bas sauvegarde une copie du tableau `myforex2` comme `forex_prices.csv` dans le dossier `~/example`.

{{%notice note%}}
Le raccourci `~/` fait référence à votre dossier `home` qui se trouve sous `/home/ulaval/votreutilisateur/`. Cette syntaxe est d'usage courant dans des systèmes de type unix <a href="https://en.wikipedia.org/wiki/Unix-like" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> comme celui utilisé par le service `WRDS` <a href="https://wrds-www.wharton.upenn.edu/pages/support/support-articles/general-information/accessing-wrds/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.
{{%/notice%}}

```
PROC EXPORT DATA=myforex2
     OUTFILE="~/example/forex_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

# Exemple complet
Le code `SAS` ci-bas créera trois fichiers `CSV` à partir de trois tableaux `WRDS` à l'aide de `SAS/Studio`. Les fichiers résultats contiennent les taux de change obtenus à partir du tableau `FRB.FX_MONHTLY`, les prix des indices FTSE du tableau `COMP.G_IDX_MTH` et leurs noms qui se trouvent sur le tableau `COMP.G_NAMES_IX`. Enfin, il suffit de faire un clic droit sur le fichier à télécharger.

![](csv_files.png)

Les fichiers `CSV` résultants peuvent être ouverts à partir de tout logiciel.

![](csv_view.png)

## Code SAS

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Créer le tableau `myforex` :

```sas
PROC SQL;
     CREATE TABLE myforex AS
     SELECT 'date'n , excaus , exchus , exhkus ,
            exinus , exjpus , exkous ,exmxus , exukus
     FROM FRB.FX_MONTHLY
     WHERE date > '01JAN2000'd;
QUIT;

```

2. Modifier le `format` des `dates` au besoin :

```sas
/* Date format */
DATA myforex2;
  SET myforex;
  FORMAT date DDMMYY10.;
RUN;
```

3. Exporter le tableau `myforex2` comme `forex_prices.csv` :

```sas
/* Export to CSV */
PROC EXPORT DATA=myforex2
	 OUTFILE="~/example/forex_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% tab "index_names.csv" %}}


1. Créer le tableau `gvnames` :

```sas
PROC SQL;
     CREATE TABLE gvnames AS
     SELECT *
     FROM COMP.G_NAMES_IX
     WHERE gvkeyx IN ('150004', '150979', '150984', '150994');
QUIT;
```

2. Exporter le tableau `gvnames` comme `index_names.csv` :

```sas
PROC EXPORT DATA=gvnames
	 OUTFILE="~/example/index_names.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% tab "index_prices.csv" %}}


1. Créer le tableau `myindex` :

```sas
PROC SQL;
     CREATE TABLE myindex AS
     SELECT gvkeyx, datadate, prccm, prchm, prclm
     FROM COMPD.G_IDX_MTH
     WHERE gvkeyx IN ('150004', '150979', '150984', '150994') AND
     datadate > '01JAN2000'd;
QUIT;
```

2. Exporter le tableau `myindex` comme `index_prices.csv` :

```sas
PROC EXPORT DATA=myindex
	 OUTFILE="~/example/index_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% /tabs %}}


