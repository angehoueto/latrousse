---
title: Python
keywords: wrds, python, connexion python, requête avec python, connexion wrds
description: "WRDS : Comment se connecter via Python."
weight: 0
---

{{%notice note%}}
Vous pouvez télécharger `Python` sur le site officiel à l'adresse `https://www.python.org`. Cependant, il est plutôt recommandé d'installer [Anaconda](https://www.anaconda.com/products/individual) qui inclut `python` ainsi que d'autres outils populaires pour l'analyse de données tel que `Jupyter Notebook`.
{{%/notice%}}

# Connexion

1. Installez le module `wrds` à l'aide de `pip`. Pour suivre ces instructions, vous pouvez utiliser  `Jupyter Notebook` {{%extlink "https://docs.anaconda.com/ae-notebooks/user-guide/basic-tasks/apps/jupyter/"%}} ou le service `WRDS Cloud` {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python"%}} .

{{% tabs %}}

{{% tab "Terminal" %}} ![](install_wrds.gif) {{% /tab %}}
{{% tab "Jupyter Notebook" %}} ![](install_wrds_jupyter.gif) {{% /tab %}}

{{% /tabs %}}

2. Créez une connexion à `wrds` avec la fonction `wrds.Connection()`. C'est aussi recommandé de créer un fichier nommé `.pgpass` dans votre dossier `home`, vous pouvez le créer manuellement ou à l'aide de la fonction `db.create_pgpass_file()`. Le ficher doit avoir la ligne suivante où vous devez remplacer `wrds_username` et `wrds_password` par votre nom d'utilisateur et votre mot de passe WRDS.

```
wrds-pgdata.wharton.upenn.edu:9737:wrds:wrds_username:wrds_password
```

{{%notice info%}}
L'emplacement de votre dossier `home` ou `~/` dépend de votre système d'exploitation. Sur des systèmes de type Unix, ce dossier se trouve à `/Users/votreutilisateur` (sur MAC) ou à `/home/votreutilisateur`. Sur Windows, il s'agit du dossier `%APPDATA%` qui se trouve à un endroit semblable à l'adresse `C:\Users\yourusername\AppData\Roaming`.
{{%/notice%}}

{{% tabs %}}

{{% tab "Terminal" %}}
![](pgpass.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](pgpass_jupyter.gif)
{{%notice note%}}
Si vous créez le fichier `.pgpass` manuellement sur Windows, évitez d'utiliser le Bloc-notes car vous devrez portez une attention accrue à l'extension du fichier créé. Au lieu du Bloc-notes, essayez [Notepad++](https://notepad-plus-plus.org/downloads/) (licence *open source*) ou [Sublime Text](https://www.sublimetext.com/) (licence propriétaire) .
{{%/notice%}}
{{% /tab %}}

{{% /tabs %}}

{{%notice info%}}
Si vous avez créé votre fichier `.pgpass` manuellement, assurez-vous de modifier les autorisations du fichier tel qu'indiqué sur la documentation de `PostreSQL` {{%extlink "https://www.postgresql.org/docs/9.3/libpq-pgpass.html"%}} . Ignorez ce message si vous travaillez sur Windows.
{{%/notice%}}


Testez votre connexion en exécutant la requête `SQL` suivante :

```python
df = db.raw_sql('SELECT date,dji FROM djones.djdaily LIMIT 5;')  # LIMIT the number of records
```

Vous devriez retrouver les `5` premières lignes du tableau `djones.daily` dans la variable `data`.

{{% tabs %}}
  {{% tab "Terminal" %}}
  ![](test_wrds.gif)
  {{% /tab %}}

  {{% tab "Jupyter Notebook" %}}
  ![](test_wrds_jupyter.gif)
  {{% /tab %}}

{{% /tabs %}}


{{%notice tip%}}
Plusieurs exemples de requête sont disponibles sur le site web du service WRDS {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/querying-wrds-data-python"%}} .
{{%/notice%}}


