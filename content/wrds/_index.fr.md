---
title: "WRDS"
date: 2020
icon: "ti-server"
description: Le service Wharton Research Data Services (WRDS) est offert en ligne par The Wharton School, University of Pennsylvania.
keywords: wrds, Wharton Research Data Services, sas, compustat, crsp
type : "docs"
---

