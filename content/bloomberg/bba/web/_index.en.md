---
title: "Web Access"
date: 2020
icon: "ti-pulse"
description: You can temporarily access a web based Bloomberg terminal using Bloomberg's Disaster Recovery Services
keywords: bloomberg, terminal, bloomberg login, bmc login, bmc, bloomberg certification, terminal login, drs, disaster
---

You first need to create a `BMC` login then request a `terminal`  login through the `BMC` portal.

{{%notice note%}}
You can *temporarily* access a web based Bloomberg terminal using Bloomberg's [Disaster Recovery Services](https://www.bloomberg.com/professional/solution/disaster-recovery-services/).
{{%/notice%}}

## BMC login

1. Go to `https://portal.bloombergforeducation.com/register` and register for a new account using your `@ulaval.ca` e-mail address.

![](register.gif)

{{%notice info%}}
If you are asked for your credit card information, please e-mail Bloomberg instead at `bbg.edu@bloomberg.net` to avoid charges.
{{%/notice%}}

2. Check your **University e-mail** and confirm your registration by clicking the link sent to your `@ulaval.ca` account.

3. Once your registration has been completed, you can log in at `https://portal.bloombergforeducation.com` .

![](login.gif)


## Terminal login

{{%notice info%}}
Before being able to create a terminal login, you need to contact your University to request access to this feature. Université Laval students must send an e-mail to `assistance.sdm@fsa.ulaval.ca`.
{{%/notice%}}

1. Log onto the BMC portal {{%extlink "https://portal.bloombergforeducation.com/"%}} using your `@ulaval.ca` e-mail address.

![](login.gif)

2. Click on the `Terminal Access`  tab to request a new terminal login.

![](terminal_access.gif)

{{%notice info%}}
If you did use your `@ulaval.ca` account, but still cannot see the `Terminal Access` tab, please contact us by e-mail at `assistance.sdm@fsa.ulaval.ca`.
{{%/notice%}}

3. Wait for a Bloomberg representative to contact you. You will then receive a `Terminal` login which you can use to access the Bloomberg terminal.

## Connecting to the Terminal

{{%notice info%}}
It is **forbidden** to log off another student **unless you have booked a terminal in advance**. Students at Université Laval must book a terminal on the FSA ULaval website {{%extlink "http://applications.fsa.ulaval.ca/salle3324/"%}} .
{{%/notice%}}

![](dontkick.png)

1. Go to `https://bba.bloomberg.net` and login with your newly created `Terminal login`.

{{%notice note%}}
You need to use your `Terminal` login, which is **not the same** as your `BMC` login.
{{%/notice%}}

![](bba_login.gif)

2. Click on the arrow next to `LAUNCH` and then on `Launch within the browser`. You are now connected to the **Bloomberg** through your web browser.

{{%notice tip%}}
It's recommended to install [Citrix](https://bba.bloomberg.net/Help?section=requirements) to use Bloomberg Anywhere instead of the web version. Just click on `LAUNCH` instead of the arrow next to the button. Citrix is available for Windows, MAC and Linux {{%extlink "https://bba.bloomberg.net/Help?section=requirements"%}}.
{{%/notice%}}
