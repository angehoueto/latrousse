---
title: Excel files
keywords: local data, historical data, save table, export table, bba download, blooomberg anywhere, excel bloomberg
description: How to download or upload an Excel file using the web version of Bloomberg.
weight: 0
---

If you are using a Bloomberg terminal, Excel files obtained from Bloomberg are automatically saved to the `C:\blp\data` folder (or something similar) on **your computer**. You can save a copy to another folder by clicking on `File` and then on `Save as` as you usually would with any Excel file.

{{%notice note%}}
If you are using Bloomberg's web version, you need to follow the instructions bellow to save a copy of a file on **your computer**.
{{%/notice%}}

## Web version

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to the Excel file you exported from Bloomberg and click on `File` and then on `Save as`[^1] to save a copy of the current file. Note that the file is **not yet** on your computer.

[^1]: You can also press the `<F12>` key to open the `Save as` window.

2. Click on the **Citrix** menu button on top of the screen and then on `Download`.

3. Choose the file you'd like to download and click on `Open`. You will find the downloaded file in your `Downloads` folder.

{{%/tab%}}
{{%tab "Terminal"%}}

![](hp_export.gif)

{{%/tab%}}
{{%/tabs%}}
