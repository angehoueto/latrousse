---
title: End key
keywords: key, end, return, keyboard, back, previous, bloomberg
description: Customizing the behaviour of the End/Fin key.
weight: 0
---

## Default behaviour

The default behavior of the `<End>` key takes you back to the previous page and then to the terminal home page. If you prefer to navigate using the function history, follow these instructions:

{{%tabs%}}
{{%tab "Instructions"%}}

1. Access the default settings by entering `PDFU 10` in the command line followed by the `<Enter>` key.

2. Click on `On` to activate the navigation through function history.

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-pdfu.gif)

{{%/tab%}}


{{%tab "?"%}}

{{%notice tip%}}
You can learn more about the Bloomberg keyboard through the Help. Type `LPHP STOP: 0: 1 2614234` followed by the `<Enter>` key. A quick start guide is available through the `UNI` function under the `Resources` section.
{{%/notice%}}

![](clavier-help.gif)

{{%/tab%}}

{{%/tabs%}}


