---
title: Historical returns
keywords: historical returns, excel data, api, bloomberg api
description: "Downloading historical returns data with Bloomberg via Excel"
weight: 0
---

It is possible to access historical data fields such as a stock's total return (including dividends) using the Bloomberg API via Excel. For this, you will need to choose a financial security and extract the  `DAY_TO_DAY_TOT_RETURN_GROSS_DVDS` field.

{{%notice tip%}}
Use the `FLDS` function on the terminal to find additional data fields. For instance, the `DAY_TO_DAY_TOT_RETURN_NET_DVDS` mnemonic corresponds to the *RT113 - Day to Day Total Return (Net Dividends)* field and additional details are available via `FLDS`.
{{%/notice%}}

## API via Excel

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on the `Bloomberg` tab and then on `Spreadsheet Builder`.
2. Under `Analyze Historical Data` : click on `Historical Data Table` and then on `Next`.
3. Choose the financial securities whose data you are interested in.
4. Choose the data fields of your interest.
5. Enter the start and end dates for the dataset as well as the frequency of the observations.
6. Choose the Excel cell where you want to put your data and click on `Finish`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](excel.gif)

{{%/tabs%}}
{{%/tabs%}}

