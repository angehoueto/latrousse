---
title: Finding a stock
keywords: eqs, stock screening, equity screening, stocks, screening, finding stocks
description: Using filters to find stocks on Bloomberg.
weight: 0
---

The Equity Screening function (`EQS`) helps us in finding stocks satisfying specific criteria (market, sector, financial ratios, etc.).

## EQS Function

{{%tabs%}}
{{%tab "Instructions"%}}

1. From the Bloomberg command line, type `EQS` and press the `<Enter>` key.
2. From the `EQS` home page, choose the filters that you would like to apply.

> At this step, you can add custom criteria and use logical operators to narrow your search.

3. After applying some filters, click on `1) Results` to get a list of equities according to your research.
4. You can also export the results to Excel if needed.

{{%/tab%}}
{{%tab "Terminal"%}}

![](eqs.gif)

{{%/tab%}}
{{%/tabs%}}

