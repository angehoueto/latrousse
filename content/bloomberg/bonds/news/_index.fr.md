---
title: Nouvelles
keywords: nouvelles sur titre, nouvelles, obligations, revenu fixe, trouver nouvelles
description: Comment trouver des nouvelles sur un titre obligataire avec Bloomberg.
weight: 0
---

La fonction `CN` nous permet d'accéder à des nouvelles concernant une entreprise.

{{%notice note%}}
Par défaut, les nouvelles affichées s'appliquent au ticker affiché en haut à gauche de l'écran Bloomberg.
{{%/notice%}}

## Fonction CN

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le ticker d'une entreprise/obligation d'intérêt et appuyez sur la touche `<Entrée>`.

2. Depuis la ligne de commande Bloomberg, tapez `CN` et appuyez sur la touche `<Entrée>`.

3. Appliquez des filtres à vos résultats, vous pouvez par exemple choisir les dates de début et fin pour votre recherche.

4. Pour sauvegarder une nouvelle, cliquez sur `Actions` et copiez le lien d'accès web direct (si disponible) ou sélectionnez `download`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](cn.gif)

{{%/tab%}}
{{%/tabs%}}

