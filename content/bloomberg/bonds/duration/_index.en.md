---
title: Duration
keywords: bond duration, bonds, fixed revenue
description: Finding a bond's duration on Bloomberg.
weight: 0
---

The `DURA` function offers access to a duration analysis for a given fixed income security.

## DURA Function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter a bond's ticker and hit `<Enter>`

2. Enter `DURA` on the Bloomberg command line and hit `<Enter>`.

{{%notice tip%}}
Press the `<F1>` key to access more details on this function. The `Definitions` section is a glossary.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](dura.gif)

{{%/tab%}}
{{%/tabs%}}

## YAS Function

The `YAS` function offers access to a *Yield ans Spread Analysis* for fixed income securities. It is also possible to find the Macaulay duration as well as the modified duration of a bond.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter a bond's ticker and hit `<Enter>`

2. Enter `YAS` on the Bloomberg command line and hit `<Enter>`.

> The Macaulay and modified duration are located on the `Risk` section.

{{%notice tip%}}
Press the `<F1>` key to access more details on this function. The `Definitions` section under `Reference` is a glossary.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](yas.gif)

{{%/tab%}}
{{%/tabs%}}

## Via Excel

It is possible to get the Macaulay and modified duration data through Excel by downloading the `DUR_MID` and `DUR_ADJ_MID` data fields.

{{%notice info%}}
Only the current data is available (no historical data). This values are the same found through the `YAS` or `DURA` functions.
{{%/notice%}}





