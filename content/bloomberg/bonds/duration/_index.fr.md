---
title: Durée
keywords: durée d'une obligations, obligations, revenu fixe, trouver durée
description: Comment trouver la durée d'une obligation avec Bloomberg.
weight: 0
---

La fonction `DURA` nous permet d'accéder à l'analyse sur la durée d'un titre obligataire.

## Fonction DURA

{{%tabs%}}
{{%tab "Instructions"%}}

1. Assurez-vous que le nom de l'obligation qui vous intéresse soit chargé en haut à gauche de l'écran Bloomberg. Si ce n'est pas le cas, rentrez le ticker de l'obligation et appuyez sur `<Entrée>`

2. Depuis la ligne de commande Bloomberg, tapez `DURA` et appuyez sur la touche `<Entrée>`.

{{%notice tip%}}
Appuyez sur la touche `<F1>` pour accéder aux détails sur cette fonction. La section `Definitions` est un glossaire.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](dura.gif)

{{%/tab%}}
{{%/tabs%}}

## Fonction YAS

La fonction `YAS` nous permet d'accéder à l'analyse de rendement/écart relatif aux titres obligataires (*Yield ans Spread Analysis*). Il est également possible d'obtenir la durée de Macaulay et la durée modifiée d'un obligation.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Assurez-vous que le ticker de l'obligation qui vous intéresse soit chargé en haut à gauche de l'écran Bloomberg.

2. Depuis la ligne de commande Bloomberg, tapez `YAS` et appuyez sur la touche `<Entrée>`.

> Vous retrouvez la durée de Macaulay et la durée modifiée dans la section `Risk`.

{{%notice tip%}}
Appuyez sur la touche `<F1>` pour accéder aux détails sur cette fonction. La section `Definitions` sous `Reference` est un glossaire.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](yas.gif)

{{%/tab%}}
{{%/tabs%}}

## Via Excel

Il est possible d'obtenir la durée de Macaulay et la durée modifiée d'une obligation sur Excel en faisant l'extraction des champs `DUR_MID` et `DUR_ADJ_MID` respectivement.

{{%notice info%}}
Seul les données courantes sont disponibles (pas d'historique) et celles-ci correspondent aux mêmes valeurs que l'on retrouve à l'aide de la fonction `YAS` et `DURA`.
{{%/notice%}}





