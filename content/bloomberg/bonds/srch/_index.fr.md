---
title: Recherche
keywords: recherche d'obligations, obligations, revenu fixe, trouver obligations,
description: Comment trouver des obligations avec Bloomberg.
weight: 0
---

La fonction `SRCH` vous permet d'appliquer des filtres (marché, secteur, coupon, durée, cote de crédit, etc.) à votre recherche d'obligations.

## Fonction `SRCH`

{{%tabs%}}
{{%tab "Instructions"%}}

1. Tapez `SRCH` sur la ligne de commande Bloomberg et appuyez sur la touche `<Entrée>` pour accéder à la fonction `Fixed Income Search`.

2. Choisissez le `Country of Risk`, `Maturity type`, `Pricing Date` ou tout autre critère de recherche disponible.

{{%notice tip%}}
L'onglet *Example Searches* permet d'accéder à plusieurs exemples de requêtes.
{{%/notice%}}

3. Cliquez sur `Results` pour obtenir la liste de titres correspondant à vos critères de recherche.

4. Cliquez sur une obligation pour avoir son **ticker Bloomberg** ainsi que pour avoir accès à d'autres fonctions relatives au titre.

> Vous pouvez trier vos résultats en cliquant sur le libellé de chaque colonne. Par exemple, en cliquant sur la colonne `Maturity Type` les données seront arrangées selon celle-ci.

{{%notice tip%}}
Pour vérifier la date de début de l'historique d'un titre, tapez `GP` et appuyez sur la touche `<Entrée>`, ensuite cliquez sur le bouton `Max`.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](srch.gif)

{{%/tab%}}
{{%/tabs%}}

## Fonction `RELS`

La fonction `RELS` vous permet de trouver des titres émis par une compagnie. Si vous avez déjà choisi une compagnie, vous pouvez vous servir de cette fonction pour trouver des obligations corporatives émises par celle-ci.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Tapez le nom de la compagnie depuis la ligne de commande Bloomberg et cliquez sur son ticker.

2. Assurez-vous que le nom de la compagnie qui vous intéresse se trouve en haut, à gauche de l'écran: cela signifie que le titre est chargé sur l'écran Bloomberg.

3. Tapez `RELS` et appuyez sur la touche `<Entrée>` pour accéder à la page d'accueil de la fonction `Related Securities`.

4. Depuis la page d'accueil de la fonction `RELS`, dans la sous-section `Debt`, sélectionnez `Bonds & Generic Tickers`.

5. Filtrez et triez par colonne à l'aide des flèches disponibles sur la première ligne du tableau.

6. Après avoir choisi une obligation, il vous suffit de cliquez sur celle-ci pour la charger sur le terminal Bloomberg et ainsi obtenir son **ticker** via la fonction `DES`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](rels.gif)

{{%/tab%}}
{{%/tabs%}}

